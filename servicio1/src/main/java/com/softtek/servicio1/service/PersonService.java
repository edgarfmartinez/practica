package com.softtek.servicio1.service;

import com.netflix.ribbon.proxy.annotation.Http;
import com.softtek.servicio1.model.Person;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

    private List<Person> people;

    public PersonService() {
        people = new ArrayList<>();
        people.add(new Person(1, "Edgar", "Martinez", 19, 'M'));
        people.add(new Person(2, "Fernando", "Andrade", 21, 'M'));
        people.add(new Person(3, "Jose", "Quezada", 29, 'M'));
        people.add(new Person(4, "Laura", "Flores", 23, 'F'));
        people.add(new Person(5, "Yesica", "Serna", 22, 'F'));
    }

    public ResponseEntity getPeople(){
        return new ResponseEntity<>(people, HttpStatus.OK);
    }

}
