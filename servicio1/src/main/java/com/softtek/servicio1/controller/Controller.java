package com.softtek.servicio1.controller;

import com.softtek.servicio1.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Autowired
    private PersonService personService;

    @GetMapping("/people")
    public ResponseEntity getPeople(){
        return personService.getPeople();
    }

}
