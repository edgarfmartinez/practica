package com.softtek.servicio2.service;

import com.softtek.servicio2.model.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeService {

    private List<Employee> employees;

    public EmployeeService(){
        employees = new ArrayList<>();
    }

    public ResponseEntity getEmpoyees(){
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

}
