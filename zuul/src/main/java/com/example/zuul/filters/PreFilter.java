package com.example.zuul.filters;

import javax.servlet.http.HttpServletRequest;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

public class PreFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        MessageLog.message = "HTTP_METHOD: " + request.getMethod()
                + ", URL: " + request.getRequestURL().toString()
                + ", Arguments: " + getParameters(request);
        return null;
    }

    private String getParameters(HttpServletRequest request){
        Enumeration<String> parameters = request.getParameterNames();
        List<String> params = new ArrayList<String>();
        String args = "[ ";
        while (parameters.hasMoreElements()) {
            String paramName = parameters.nextElement();
            String[] paramValues = request.getParameterValues(paramName);
            for (int i = 0; i < paramValues.length; i++) {
                params.add(paramValues[i]+", ");
            }
        }
        for(int i = params.size() - 1; i >= 0; i--)
            args += params.get(i);
        args += "],";
        return args;
    }

}
